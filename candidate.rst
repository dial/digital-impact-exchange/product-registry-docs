Submitting a Candidate Product 
==============================

.. meta::
   :description lang=en:

Users may submit information about a new product for consideration to be added to the Exchange. 
On the products page at the top of the list of products, there is a link for 'Create New'. This
link will take the user to a page where they can submit the name, website, code repository,
and contact email address for the new product. Once this information has been submitted, it 
will be reviewed by a Exchange administrator. If the product meets the criteria for addition, it
will be added to the Exchange.

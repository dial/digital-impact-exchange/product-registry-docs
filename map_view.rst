Map View
========

.. meta::
   :description lang=en:

The Exchange has three interactive maps that provide an visualizations of projects,
organizations that have endorsed the Digital Principles, and mobile network aggregators. 

The Projects map provides a list of implementations of products in different country contexts.
The map shows the number of projects in each country. Click on the number indicator to see
a list of all of the project names. Click on a specific project to get detailed information
about the project. Projects can also be filtered by sector or by a specific tag. Select a 
sector or tag to see a subset of projects. 

The map of Digital Principles Endorers allows users to see the main office of each organization.
The user can select an organization to see what countries they work in. A user can also filter
the organizations shown by the year in which they endorsed the Digital Principles and/or which
development sectors they work in.

The map of Mobile Network Aggregators shows which aggregators work in which countries around 
the world. The user can filter the map by a specific aggregator, or can select a specific mobile
network operator to see which countries the aggregator or operator work in. The user can also
filter by a specific type of mobile service offered (it will show the countries in which that 
service is offered)

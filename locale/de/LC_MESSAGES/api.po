# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, Digital Impact Alliance
# This file is distributed under the same license as the DIAL Online Catalog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Steven Conrad <sconrad@digitalimpactalliance.org>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: DIAL Online Catalog\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-17 08:13-0500\n"
"PO-Revision-Date: 2021-06-28 21:48+0000\n"
"Last-Translator: Steven Conrad <sconrad@digitalimpactalliance.org>, 2021\n"
"Language-Team: German (https://www.transifex.com/digital-impact-alliance/teams/109854/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: /Users/sconrad/DIAL/product-registry-docs/api.rst:2
msgid "API Documentation"
msgstr "API-Dokumentation"

#: /Users/sconrad/DIAL/product-registry-docs/api.rst:7
msgid ""
"For developers or organizations that wish to use the data in the online "
"catalog, we have made API documentation available using Swagger/OpenAPI."
msgstr ""
"Für Entwickler und Organisationen, welche die Daten im Online-Katalog "
"verwenden möchten, steht eine API-Dokumentation mittels Swagger/OpenAPI zur "
"Verfügung."

#: /Users/sconrad/DIAL/product-registry-docs/api.rst:10
msgid ""
"To view the API documentation, navigate to the `api-docs page`_ within the "
"online catalog."
msgstr ""
"Um die API-Dokumentation zu sehen, navigieren Sie im Online-Katalog zur "
"‚api-docs Seite‘_."

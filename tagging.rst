Tagging Objects
===============

.. meta::
   :description lang=en:

In addition to filtering, use cases and products can be tagged. Tagging allows users
to organize these objects by particular characteristics and provides another 
powerful and flexible tool for organizing data in the Exchange to align with users 
needs.


Configuring active tags
-----------------------

Tags must be defined before they can be attached to a product or use case. Users with 
administrator privileges can create, edit and delete tags. Navigate to the Tags page 
from the Admin menu. In this page, a user can create a new tag, edit an existing tag,
or delete a tag that is no longer needed.


Tagging a Use Case or product
-----------------------------

When creating or editing a product or use case, a user can assign a tag to that object. 
Enter the name of the tag in the tags field and select the tag that is to be attached 
to the object. 


Searching by Tags
-----------------

In the filter section for products, a user can enter the name of a tag. The results list
will be updated to show only products that contain that tag.
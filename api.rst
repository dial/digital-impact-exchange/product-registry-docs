API Documentation
=================

.. meta::
   :description lang=en:

For developers or organizations that wish to use the data in the Exchange, we have
made API documentation available using Swagger/OpenAPI.

To view the API documentation, navigate to the `api-docs page`_ within the Exchange

.. _api-docs page: https://exchange.dial.global/api-docs
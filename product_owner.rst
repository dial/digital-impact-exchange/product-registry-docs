Becoming a Product or Open Data Owner
=====================================

.. meta::
   :description lang=en:

If a user is associated with a particular product or open data resource in the Exchange, that user
can sign up to be the 'owner' of that product or open data. An owner can update information
about the product or dataset, including information on where the product has been deployed or used (projects).

A user can become a product or open data owner only if they have an account on the Exchange. Once
the user has an account, they can click on the 'Become a Product Owner' link on the product or open data
detail page. The Exchange team will go through a vetting process (including ensuring that the account email
matches the product website URL), and then they will be registered as an owner for that product. 

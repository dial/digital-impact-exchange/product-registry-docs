Becoming an Organization Owner
==============================

.. meta::
   :description lang=en:


If a user is associated with a particular organization in the Exchange, that user can sign 
up to be an organization owner. An organization owner can update information
about the organization, including information projects that the organization has worked
on as well as sectors and countries that the organization works in.

A user can become an organization owner when they create a new account on the Exchange. 
Click on the 'Sign Up' link at the top and provide an email address and password for the
account. **Note: the email address must have the same domain name as the website that is
associated with the organization**. For example, if the website of the organization is www.organization.org,
then the product owner email address must be <name>@organization.org.

If the user already has an account, they can click on the 'Become an Organization Owner' link
on the organization  detail page. If their account email matches the organization website URL, they
will be registered as an owner for that organization.

# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, Digital Impact Alliance
# This file is distributed under the same license as the DIAL Online Catalog package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Steven Conrad <sconrad@digitalimpactalliance.org>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: DIAL Online Catalog\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-17 08:13-0500\n"
"PO-Revision-Date: 2021-06-28 21:48+0000\n"
"Last-Translator: Steven Conrad <sconrad@digitalimpactalliance.org>, 2021\n"
"Language-Team: French (https://www.transifex.com/digital-impact-alliance/teams/109854/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: /Users/sconrad/DIAL/product-registry-docs/candidate.rst:2
msgid "Submitting a Candidate Product"
msgstr "Soumettre un produit candidat"

#: /Users/sconrad/DIAL/product-registry-docs/candidate.rst:7
msgid ""
"Users may submit information about a new product for consideration to be "
"added to the catalog. On the products page at the top of the list of "
"products, there is a link for 'Submit Candidate'. This link will take the "
"user to a page where they can submit the name, website, code repository, and"
" contact email address for the new product. Once this information has been "
"submitted, it will be reviewed by a Catalog administrator. If the product "
"meets the criteria for addition, it will be added to the catalog."
msgstr ""
"Les utilisateurs peuvent soumettre des informations sur un nouveau produit "
"afin que celui-ci soit ajouté au catalogue. Sur la page des produits, en "
"haut de la liste des produits, vous trouverez le lien « Soumettre le "
"candidat ». Ce lien dirigera l’utilisateur vers une page où il pourra "
"soumettre le nom, le site Web, le référentiel de codes et l’adresse e-mail "
"de contact pour le nouveau produit. Une fois que ces informations ont été "
"soumises, elles seront examinées par un administrateur du catalogue. Si le "
"produit répond aux critères d’ajout, il sera ajouté au catalogue."

Exporting Data
==============

.. meta::
   :description lang=en:

When viewing a list of Use Cases, Workflows, Building Blocks, Products, Organizations, or 
Projects, a user can export that list into a machine readable format. At the top of the list,
there are two links for exporting the data listed in either a json or comma-separated format.
Click on the link and the data will be downloaded to the user's machine.

Note: Users must have an account and be logged in to access the export functionality

Users can also view the :doc:`API Documentation <api>` to see how they can read data from 
the Exchange by calling these APIs directly. 